/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gabriel
 */
public class Pratica32 {
    public static double densidade(double x, double media, 
            double desvio) {
        double d = Math.exp(-Math.pow(((x-media)/desvio),2)/2)/(desvio*Math.sqrt(2*Math.PI));
        return d;
    }
    public static void main(String[] args) {
        double dens=densidade(-1,67,3);
        System.out.println(dens);
    }
}
